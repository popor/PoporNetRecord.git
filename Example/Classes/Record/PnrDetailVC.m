//
//  PnrDetailVC.m
//  PoporNetRecord
//
//  Created by popor on 2018/5/16.
//  Copyright © 2018年 popor. All rights reserved.

#import "PnrDetailVC.h"

#import "PnrPortEntity.h"
#import "PnrConfig.h"
#import "PnrUITool.h"

static CGFloat kDelayTime = 0.05;
static NSMutableAttributedString * kAttLoading = nil;

@interface PnrDetailVC () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak  ) PnrPortEntity        * portEntity;
@property (nonatomic, weak  ) PnrConfig            * config;
@property (nonatomic        ) BOOL                   show;
@property (nonatomic        ) BOOL                   delay;

@end

@implementation PnrDetailVC
@synthesize infoTV;
@synthesize jsonArray;
@synthesize titleArray;
@synthesize cellAttArray;
@synthesize selectRow;
@synthesize menu;
@synthesize blockExtraRecord;
@synthesize weakPnrEntity;

- (instancetype)initWithDic:(NSDictionary *)dic {
    if (self = [super init]) {
        if (dic) {
            self.title        = dic[@"title"];
            
            self.jsonArray    = dic[@"jsonArray"];
            self.titleArray   = dic[@"titleArray"];
            self.cellAttArray = dic[@"cellAttArray"];
            
            
            self.blockExtraRecord = dic[@"blockExtraRecord"];
            self.weakPnrEntity    = dic[@"weakPnrEntity"];
        }
        self.portEntity = [PnrPortEntity share];
    }
    return self;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    if (self.menu) {
        [self.menu setMenuVisible:NO];
        self.menu = nil;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (!self.title) {
        self.title = @"PnrDetailVC";
    }
    
    self.config = [PnrConfig share];
    self.view.backgroundColor = [UIColor whiteColor];
    
    {// 预设参数
        self.show  = NO;
        self.delay = NO;
        if (kAttLoading == nil) {
            kAttLoading = [[NSMutableAttributedString alloc] initWithString:@"... loading ..."];

            [kAttLoading addAttribute:NSForegroundColorAttributeName value:[PnrUITool colorRed]   range:NSMakeRange(0, 4)];
            [kAttLoading addAttribute:NSForegroundColorAttributeName value:[PnrUITool colorGreen] range:NSMakeRange(4, 7)];
            [kAttLoading addAttribute:NSForegroundColorAttributeName value:[PnrUITool colorRed]   range:NSMakeRange(11, 4)];
        }
    }
    [self addViews];
    
    {// 延迟刷新列表
        dispatch_async(dispatch_get_main_queue(), ^{
            self.show = YES;
            [self.infoTV reloadData];
        });
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(kDelayTime * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.delay = YES;
        });
    }
}

- (void)addViews {
    self.infoTV = [self addTVs];
    self.infoTV.separatorInset = UIEdgeInsetsMake(0, 14, 0, 14);
    
    [PnrUITool layoutSameBounds:self.infoTV superView:self.view];
    
    UITapGestureRecognizer *recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGRAction:)];
    [recognizer setNumberOfTapsRequired:1];
    [recognizer setNumberOfTouchesRequired:1];
    [self.infoTV addGestureRecognizer:recognizer];
    
    {
        UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"复制" style:UIBarButtonItemStylePlain target:self action:@selector(copyAction)];
        UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithTitle:@"转发" style:UIBarButtonItemStylePlain target:self action:@selector(forwardeAction)];
        if (self.blockExtraRecord) {
            self.navigationItem.rightBarButtonItems = @[item1, item2];
        } else {
            self.navigationItem.rightBarButtonItems = @[item1];
        }
    }
}

- (UITableView *)addTVs {
    UITableView * oneTV = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    
    oneTV.delegate   = self;
    oneTV.dataSource = self;
    
    oneTV.allowsMultipleSelectionDuringEditing = YES;
    oneTV.directionalLockEnabled = YES;
    
    oneTV.estimatedRowHeight           = 0;
    oneTV.estimatedSectionHeaderHeight = 0;
    oneTV.estimatedSectionFooterHeight = 0;
    
    [self.view addSubview:oneTV];
    
    oneTV.separatorInset = UIEdgeInsetsMake(0, 15, 0, 15);
    
    return oneTV;
}

#pragma mark - 复制
// uiscrollview 会把touch事件覆盖掉,但是我不想重新定义UITableView,因为涉及到了很多runtime函数.
//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    //两句话是保存触摸起点位置
//    UITouch *touch=[touches anyObject];
//    CGPoint cureentTouchPosition=[touch locationInView:self.infoTV];
//    //得到cell中的IndexPath
//    NSIndexPath *indexPath=[self.infoTV indexPathForRowAtPoint:cureentTouchPosition];
//
//    self.selectRow = (int)indexPath.row;
//
//    [self showUIMenuAtPoint:cureentTouchPosition];
//}

- (void)tapGRAction:(UITapGestureRecognizer *)tapGR {
    CGPoint point = [tapGR locationInView:self.infoTV];
    if (point.y > self.infoTV.contentSize.height) {
        if (self.menu) {
            [self.menu setMenuVisible:NO];
            self.menu = nil;
        }
    }else{
        NSIndexPath *indexPath = [self.infoTV indexPathForRowAtPoint:point];
        self.selectRow = (int)indexPath.row;
        if (self.selectRow >=0 && self.selectRow<self.cellAttArray.count) {
            [self showUIMenuAtPoint:point];
        }
    }
}

- (void)showUIMenuAtPoint:(CGPoint)point {
    [self.view becomeFirstResponder];
    
    UIMenuItem *copyTextContentMenuItem = [[UIMenuItem alloc] initWithTitle:@"复制Text" action:@selector(copyTextContent:)];
    UIMenuItem *copyJsonContentMenuItem = [[UIMenuItem alloc] initWithTitle:@"复制Json" action:@selector(copyJsonContent:)];
    UIMenuController *menu = [UIMenuController sharedMenuController];
    
    if ([self.jsonArray[self.selectRow] isKindOfClass:[NSDictionary class]]) {
        [menu setMenuItems:@[copyTextContentMenuItem, copyJsonContentMenuItem]];
    }else{
        [menu setMenuItems:@[copyTextContentMenuItem]];
    }
    
    [menu setTargetRect:CGRectMake(point.x - 50, point.y - 30, 100, 40) inView:self.infoTV];
    [menu setMenuVisible:YES animated:YES];
    
    self.menu = menu;
}

- (void)copyTextContent:(UIMenuItem *)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    NSMutableAttributedString * att = self.cellAttArray[self.selectRow];
    [pasteboard setString:att.string];
}

- (void)copyJsonContent:(UIMenuItem *)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    NSDictionary * dic = self.jsonArray[self.selectRow];
    NSString * jsonString = [PnrUITool jsonOfDic:dic];
    if (jsonString) {
        [pasteboard setString:jsonString];
    }else{
        // AlertToastTitle(@"复制失败");
    }
}

- (BOOL)canPerformAction:(SEL)action withSender:(id)sender {
    BOOL result = NO;
    if(@selector(copyTextContent:) == action
       ||@selector(copyJsonContent:) == action
       ) {
        result = YES;
    }
    return result;
}

- (BOOL)canBecomeFirstResponder {
    return YES;
}


#pragma mark - TV_Delegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.show) {
        return 1;
    } else {
        return 0;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cellAttArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 100;
}

// 可以使用系统的自动约束,计算一遍只会增加cpu
//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
//    float width = self.view.frame.size.width;
//    NSMutableAttributedString * att = self.cellAttArray[indexPath.row];
//    static UILabel * l;
//    if (!l) {
//        l = [UILabel new];
//        l.font = self.config.cellTitleFont;
//        l.numberOfLines = 0;
//    }
//    l.frame = CGRectMake(0, 0, width-30, 10);
//
//    switch (self.config.jsonViewColorBlack) {
//        case PnrListTypeTextColor: {
//            l.attributedText = att;
//            break;
//        }
//        case PnrListTypeTextBlack: {
//            l.text = att.string;
//            break;
//        }
//        default:{
//            break;
//        }
//    }
//
//    [l sizeToFit];
//
//    return l.frame.size.height + 20;
//    //return MAX(l.frame.size.height + 6, 56);
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString * CellID = @"CellID";
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:CellID];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle :UITableViewCellStyleDefault reuseIdentifier :CellID];
        cell.selectionStyle          = UITableViewCellSelectionStyleNone;
        cell.textLabel.font          = self.config.cellTitleFont;
        cell.textLabel.numberOfLines = 0;
        cell.textLabel.lineBreakMode = NSLineBreakByClipping;
    }
    NSMutableAttributedString * att = self.cellAttArray[indexPath.row];
    
    switch (self.config.jsonViewColorBlack) {
        case PnrListTypeTextColor: {
            if (att.string.length > 400) {
                if (self.delay) {
                    cell.textLabel.attributedText = att;
                } else {
                    cell.textLabel.attributedText = kAttLoading;
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((kDelayTime + 0.02) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                    });
                }
            } else {
                cell.textLabel.attributedText = att;
            }
            break;
        }
        case PnrListTypeTextBlack: {
            if (att.string.length > 400) {
                if (self.delay) {
                    cell.textLabel.text = att.string;
                } else {
                    cell.textLabel.text = kAttLoading.string;
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)((kDelayTime + 0.02) * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
                    });
                }
            } else {
                cell.textLabel.text = att.string;
            }
            break;
        }
        default:{
            break;
        }
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView == self.infoTV) {
        if (self.menu) {
            [self.menu setMenuVisible:NO];
            self.menu = nil;
        }
    }
}

#pragma mark - VC_EventHandler
- (void)copyAction {
    NSMutableString * text = [NSMutableString new];
    for (NSMutableAttributedString * att in self.cellAttArray) {
        [text appendFormat:@"%@\n\n", att.string];
    }
    
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    [pasteboard setString:text];
    
    // AlertToastTitle(@"已复制");
    
    UIActivityViewController *activity = [[UIActivityViewController alloc] initWithActivityItems:@[text] applicationActivities:nil];
    
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        [self presentViewController:activity animated:YES completion:nil];
    }else{
        [self.navigationController pushViewController:activity animated:YES];
    }
}

- (void)forwardeAction {
    if (self.blockExtraRecord) {
        self.blockExtraRecord(self.weakPnrEntity);
        
        // AlertToastTitle(@"已转发");
    }
}

@end
