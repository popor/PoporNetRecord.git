//
//  PoporNetRecord.h
//  PoporNetRecord
//
//  Created by popor on 2018/5/16.
//  Copyright © 2018年 popor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PnrPrefix.h"
#import "PnrConfig.h"
#import "PnrEntity.h"
#import "PnrExtraEntity.h"

NS_ASSUME_NONNULL_BEGIN

@interface PoporNetRecord : NSObject

@property (nonatomic, weak  ) PnrConfig * config;

@property (nonatomic, strong) NSMutableArray * infoArray;
@property (nonatomic, weak  ) UINavigationController * nc;

@property (nonatomic, copy  ) PnrBlockPPnrEntity blockExtraRecord; // 转发完成之后的回调


// 网络请求频率监测
@property (nonatomic        ) CGFloat             requestRatePerSecond; //  每秒多少次
@property (nonatomic, copy  ) PnrBlockPFloatFloat blockException;   // 网络异常回调


+ (instancetype)share;

// 网络请求部分
/**
 headValue:      NSDictionary | NSString
 parameterValue: NSDictionary | NSString
 responseValue:  NSDictionary | NSString
 */
+ (void)addUrl:(NSString *)urlString method:(NSInteger)method head:(id _Nullable)headValue parameter:(id _Nullable)parameterValue response:(id _Nullable)responseValue;

// 增加title
+ (void)addUrl:(NSString *)urlString title:(NSString *)title method:(NSInteger)method head:(id _Nullable)headValue parameter:(id _Nullable)parameterValue response:(id _Nullable)responseValue;

+ (void)setMethodArray:(NSArray<PnrEntityMethod *> *)array;

+ (void)setPnrBlockResubmit:(PnrBlockResubmit _Nullable)block extraDic:(NSDictionary * _Nullable)dic;

// Log 部分
+ (void)addLog:(NSString *)log;
+ (void)addLog:(NSString *)log title:(NSString *)title;

// 内部调运
+ (void)startWebServer;

// 内部调运
+ (void)stopWebServer;


/**
 *  @brief 监测网络请求频率, 假如发现连续3秒, 每秒超过perSecond, 则反馈. 最长监测4秒内.
 *
 *  @param perSecond 假如数值过小, 会导致监测的平均数异常.
 *  @param blockException 回调
 */
+ (void)watchRequestRate:(CGFloat)perSecond exception:(PnrBlockPFloatFloat)blockException;

@end

NS_ASSUME_NONNULL_END

