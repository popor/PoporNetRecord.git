//
//  PnrListVC.h
//  PoporNetRecord
//
//  Created by popor on 2018/5/16.
//  Copyright © 2018年 popor. All rights reserved.

#import <UIKit/UIKit.h>
#import "PnrEntity.h"
#import "PnrCellEntity.h"
#import "PnrPrefix.h"

static CGFloat PnrListVCConfigCellHeight = 50;

@class PnrListVC;

@interface PnrListVC : UIViewController 

// self   : 自己的
@property (nonatomic, strong) UITableView     * infoTV;

//@property (nonatomic, strong) AlertBubbleView * alertBubbleView;
//@property (nonatomic, strong) UITableView     * alertBubbleTV;
//@property (nonatomic, strong) UIColor         * alertBubbleTVColor;
//@property (nonatomic, strong) NSArray         * rightBarArray;
// inject : 外部注入的
@property (nonatomic, weak  ) NSMutableArray<PnrEntity *> * weakInfoArray;
@property (nonatomic, copy  ) PnrBlockPPnrEntity blockExtraRecord; // 转发完成之后的回调
@property (nonatomic, copy  ) PnrBlockPVoid      closeBlock;


- (instancetype)initWithDic:(NSDictionary *)dic;

+ (PnrListVC *)getPnrListVC_default;

@end
