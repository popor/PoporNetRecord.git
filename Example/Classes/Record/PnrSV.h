//
//  PnrSV.h
//  PoporNetRecord
//
//  Created by popor on 2021/3/28.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

static NSInteger PnrSV_ignore_tag = 2000;

@interface PnrSV : UIScrollView

@end

NS_ASSUME_NONNULL_END
