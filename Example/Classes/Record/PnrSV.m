//
//  PnrSV.m
//  PoporNetRecord
//
//  Created by popor on 2021/3/28.
//

#import "PnrSV.h"

@implementation PnrSV

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView * view = [super hitTest:point withEvent:event];
    //NSLog(@"view class: %@", NSStringFromClass(view.class));
    UITableViewCell * cell;
    if ([view isKindOfClass:NSClassFromString(@"UITableViewCellContentView")]) {
        cell = (UITableViewCell *)view.superview;
        //NSLog(@"view.superview class: %@", NSStringFromClass(view.superview.class));
    }
    if (cell.tag == PnrSV_ignore_tag) {
        self.scrollEnabled = NO;
    } else {
        self.scrollEnabled = YES;
    }
    return view;
}

@end
