//
//  PnrUITool.m
//  PoporNetRecord_Example
//
//  Created by popor on 2019/11/18.
//  Copyright © 2019 popor. All rights reserved.
//

#import "PnrUITool.h"

@implementation PnrUITool

+ (CGFloat)statusBarHeight {
    static CGFloat statusBarHeight;
    if (statusBarHeight == 0) {
        if (@available(iOS 13.0, *)) {
            statusBarHeight = [UIApplication sharedApplication].keyWindow.windowScene.statusBarManager.statusBarFrame.size.height;
        } else {
            statusBarHeight = [[UIApplication sharedApplication] statusBarFrame].size.height;
        }
    }
    return statusBarHeight;
}

+ (NSString *)currentTime {
    static NSDateFormatter * formatter;
    if (!formatter) {
        formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HH:mm:ss"];
        [formatter setCalendar:[[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian]];
    }
    return [formatter stringFromDate:[NSDate date]];
}

+ (NSTimeInterval)getCurrentUnixDate {
    return [[NSDate date] timeIntervalSince1970];
}

+ (NSString *)jsonOfDic:(NSDictionary *)dic {
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:kNilOptions error:nil];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}

+ (NSDictionary *)dicOfJson:(NSString *)json {
    if (json.length == 0) {
        return nil;
    }
    
    NSError *err;
    NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:[json dataUsingEncoding:NSUTF8StringEncoding] options:NSJSONReadingAllowFragments error:&err];
    if(err) {
        return nil;
    }else{
        return dic;
    }
}

+ (CGFloat)ncViewVisibleTop:(UINavigationController *)nc {
    CGFloat top = 0;
    if (nc.navigationBar.translucent) {
        top = top + nc.navigationBar.frame.size.height +[self statusBarHeight];
    }
    return top;
}

+ (CGFloat)ncViewVisibleBottom:(UINavigationController *)nc {
    CGFloat bottom = 0;
    if (nc.presentingViewController) {
        if (nc.modalPresentationStyle != UIModalPresentationFullScreen) {
            bottom = 60;
            bottom = bottom + (nc.topViewController.view.frame.size.height -nc.view.frame.size.height);
            bottom = bottom +[self safeAreaInsets].bottom;
        }
    }
    return bottom;
}

+ (UIEdgeInsets)safeAreaInsets {
    if (@available(iOS 11.0, *)) {
        UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
        return mainWindow.safeAreaInsets;
    } else {
        return UIEdgeInsetsZero;
    }
}

+ (UIColor *)colorHEX:(unsigned long)rgbValue {
    return [UIColor colorWithRed:((CGFloat)((rgbValue & 0xFF0000) >> 16))/255.0 green:((CGFloat)((rgbValue & 0xFF00) >> 8))/255.0 blue:((CGFloat)(rgbValue & 0xFF))/255.0 alpha:1.0];
}

+ (UIColor *)colorGreen {
    static UIColor * color;
    if (!color) {
        color = [self colorHEX:0X4BB748];
    }
    return color;
}

+ (UIColor *)colorRed {
    static UIColor * color;
    if (!color) {
        color = [self colorHEX:0XF76738];
    }
    return color;
}

+ (UIColor *)colorBlack3{
    static UIColor * color;
    if (!color) {
        color = [self colorHEX:0X333333];
    }
    return color;
}

+ (UIColor *)colorBlack6{
    static UIColor * color;
    if (!color) {
        color = [self colorHEX:0X666666];
    }
    return color;
}

+ (UIColor *)color0 {
    static UIColor * color;
    if (!color) {
        color = [self colorHEX:0XEEEEEE];
    }
    return color;
}
+ (UIColor *)color1 {
    static UIColor * color;
    if (!color) {
        color = [self colorHEX:0XE2E2E2];
    }
    return color;
}
+ (UIColor *)color2 {
    static UIColor * color;
    if (!color) {
        color = [self colorHEX:0XFFFFFF];
    }
    return color;
}


#pragma mark - string & att
+ (void)att:(NSMutableAttributedString * _Nullable)att
     string:(NSString * _Nullable)text
       font:(UIFont * _Nullable)font
      color:(UIColor * _Nullable)color
{
    if (!text) {
        return;
    }
    NSRange range = NSMakeRange(0, text.length);
    NSMutableAttributedString *attString = [[NSMutableAttributedString alloc] initWithString:text];
    if (font) {
        [attString addAttribute:NSFontAttributeName value:font range:range];
    }
    if (color) {
        [attString addAttribute:NSForegroundColorAttributeName value:color range:range];
    }
    //    if (bgColor) {
    //        [attString addAttribute:NSBackgroundColorAttributeName value:bgColor range:range];
    //    }
    
    [att appendAttributedString:attString];
}

+ (CGSize)sizeString:(NSString *)string font:(UIFont * _Nonnull)font width:(CGFloat)width {
    if (!string || string.length==0 || !font) {
        return CGSizeZero;
    }
    
    CGSize size = CGSizeMake(width, 200000.0f);
    NSDictionary * tdic = [NSDictionary dictionaryWithObjectsAndKeys:font, NSFontAttributeName,nil];
    
    size = [string boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin |NSStringDrawingUsesFontLeading attributes:tdic context:nil].size;
    size = CGSizeMake(ceil(size.width), ceil(size.height));
    return size;
}

#pragma mark - layout
+ (void)layoutSameBounds:(UIView *)subView superView:(UIView *)superView {
    subView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSLayoutConstraint *topLC = [NSLayoutConstraint constraintWithItem:subView attribute:NSLayoutAttributeTop relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeTop multiplier:1.0 constant:0];
    [superView addConstraint:topLC];
    
    NSLayoutConstraint *leftLC  = [NSLayoutConstraint constraintWithItem:subView attribute:NSLayoutAttributeLeft relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeLeft multiplier:1.0 constant:0];
    [superView addConstraint:leftLC];
    
    //    NSLayoutConstraint *rightLC  = [NSLayoutConstraint constraintWithItem:subView attribute:NSLayoutAttributeRight relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeRight multiplier:1 constant:0];
    //    [superView addConstraint:rightLC];
    
    NSLayoutConstraint *widthLC  = [NSLayoutConstraint constraintWithItem:subView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeWidth multiplier:1 constant:0];
    [superView addConstraint:widthLC];
    
    NSLayoutConstraint *heightLC  = [NSLayoutConstraint constraintWithItem:subView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeHeight multiplier:1 constant:0];
    [superView addConstraint:heightLC];
    
    //    NSLayoutConstraint * bottomLC  = [NSLayoutConstraint constraintWithItem:subView attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationLessThanOrEqual toItem:superView attribute:NSLayoutAttributeBottom multiplier:1 constant:0];
    //    [superView addConstraint:bottomLC];
}
@end
