//
//  PnrUITool.h
//  PoporNetRecord_Example
//
//  Created by popor on 2019/11/18.
//  Copyright © 2019 popor. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PnrUITool : NSObject

+ (CGFloat)statusBarHeight;

+ (NSString *)currentTime;

+ (NSTimeInterval)getCurrentUnixDate;

+ (NSString *)jsonOfDic:(NSDictionary *)dic;

+ (NSDictionary *)dicOfJson:(NSString *)json;

+ (CGFloat)ncViewVisibleTop:(UINavigationController *)nc;

+ (CGFloat)ncViewVisibleBottom:(UINavigationController *)nc;

+ (UIEdgeInsets)safeAreaInsets;

#pragma mark - color


#define PnrColorGreen     [PnrUITool colorGreen]
#define PnrColorRed       [PnrUITool colorRed]
#define PnrColorBlack3    [PnrUITool colorBlack3]
#define PnrColorBlack6    [PnrUITool colorBlack6]

#define PnrColor0         [PnrUITool color0]
#define PnrColor1         [PnrUITool color1]
#define PnrColor2         [PnrUITool color2]

+ (UIColor *)colorHEX:(unsigned long)rgbValue;

+ (UIColor *)colorGreen;
+ (UIColor *)colorRed;

+ (UIColor *)colorBlack3;
+ (UIColor *)colorBlack6;

+ (UIColor *)color0;
+ (UIColor *)color1;
+ (UIColor *)color2;

#pragma mark - string & att
+ (void)att:(NSMutableAttributedString * _Nullable)att
     string:(NSString * _Nullable)text
       font:(UIFont * _Nullable)font
      color:(UIColor * _Nullable)color;

+ (CGSize)sizeString:(NSString *)string font:(UIFont * _Nonnull)font width:(CGFloat)width;

#pragma mark - layout
+ (void)layoutSameBounds:(UIView *)subView superView:(UIView *)superView;

@end

NS_ASSUME_NONNULL_END
