//
//  PnrConfig.m
//  PoporNetRecord
//
//  Created by popor on 2018/11/2.
//

#import "PnrConfig.h"

static NSString * KeyTextColor = @"PoporNetRecord_textColor";
static NSString * KeyLog       = @"PoporNetRecord_logDetail";

static NSString * PoporNetRecord_recordType = @"PoporNetRecord_recordType";
static NSString * PoporNetRecord_webType    = @"PoporNetRecord_webType";


@interface PnrConfig ()
// 是否开启监测
@property (nonatomic) BOOL record;
@property (nonatomic) BOOL showListWeb;

@end

@implementation PnrConfig

+ (instancetype)share {
    static dispatch_once_t once;
    static PnrConfig * instance;
    dispatch_once(&once, ^{
        instance = [self new];
        
        instance.vcRootTitle         = @"网络请求";
        instance.webRootTitle        = @"网络请求";
        
        UIFont * font                = [UIFont systemFontOfSize:15];
        //NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
        //paraStyle.lineSpacing = 1;

        instance.cellTitleFont       = font;
        
        // ---
        instance.titleAttributes     = @{NSForegroundColorAttributeName:[UIColor blackColor], NSFontAttributeName:font};
        instance.keyAttributes       = @{NSForegroundColorAttributeName:PnrColorGreen, NSFontAttributeName:font};
        instance.stringAttributes    = @{NSForegroundColorAttributeName:PnrColorRed, NSFontAttributeName:font};
        instance.nonStringAttributes = @{NSForegroundColorAttributeName:PnrColorRed, NSFontAttributeName:font};
        
        {
            // instance.listCellHeight = 55;

            instance.listFontTitle      = [UIFont systemFontOfSize:16];
            instance.listFontRequest    = [UIFont systemFontOfSize:14];
            instance.listFontDomain     = [UIFont systemFontOfSize:15];
            instance.listFontTime       = [UIFont systemFontOfSize:15];

            instance.listColorTitle     = PnrColorGreen;
            instance.listColorRequest   = PnrColorRed;
            instance.listColorDomain    = PnrColorBlack3;
            instance.listColorTime      = PnrColorBlack6;

            instance.listColorCell0     = [UIColor whiteColor];
            instance.listColorCell1     = [UIColor colorWithRed:0.95 green:0.95 blue:0.95 alpha:1];

            instance.listWebWidth       = 25;
            instance.listWebColorCellBg = PnrColor0;
            instance.listWebColorCell0  = PnrColor1;
            instance.listWebColorCell1  = PnrColor2;
            instance.listWebColorBg     = UIColor.whiteColor;
            
            instance.rootColorKey       = PnrColorGreen;
            instance.rootColorValue     = PnrColorRed;
            [instance updateListCellHeight];
            
            instance.jsonViewColorBlack = [PnrConfig get__textColor];
            instance.jsonViewLogDetail  = [PnrConfig get__logDetail];
        }
        
        instance.segmentedControl_backgroundColor     = UIColor.clearColor;
        instance.segmentedControl_tintColor           = UIColor.whiteColor;
        instance.segmentedControl_titleColor_normal   = UIColor.blackColor;
        instance.segmentedControl_titleColor_selected = UIColor.blackColor;
       
        [instance resumeType];
    });
    return instance;
}

- (void)resumeType {
    _recordType = [self get_PoporNetRecord_recordType];
    _webType    = [self get_PoporNetRecord_webType];
    
    [self updateRecord_fromRecorType];
    [self updateShowListWeb_fromWebType];
}

- (void)setRecordTypeDefault:(PoporNetRecordType)type {
    if (self.recordType == 0) {
        self.recordType = type;
    }
}

- (void)setWebTypeDefault:(PoporNetRecordType)type {
    if (self.webType == 0) {
        if (type == PoporNetRecordAuto) {
            type = PoporNetRecordEnable;
        }
        self.webType = type;
    }
}

// 开关
- (void)setRecordType:(PoporNetRecordType)recordType {
    if (_recordType == 0 || _recordType != recordType) {
        _recordType = recordType;
        
        [self updateRecord_fromRecorType];
        [self save_PoporNetRecord_recordType:recordType];
    }
}

- (void)updateRecord_fromRecorType {
    switch (_recordType) {
        case PoporNetRecordAuto:
#if TARGET_IPHONE_SIMULATOR
            _record = YES;
#else
            //---------------- 2
#ifndef __OPTIMIZE__ //测试
            _record = YES;
#else //正式
            _record = NO;
#endif
            //---------------- 2
            
#endif
            break;
        case PoporNetRecordEnable:
            _record = YES;
            break;
            
        case PoporNetRecordDisable:
            _record = NO;
            break;
            
        default:
            break;
    }
}

- (void)setWebType:(PoporNetRecordType)listWebType {
    if (_webType == 0 || _webType != listWebType) {
        _webType = listWebType;
        
        [self updateShowListWeb_fromWebType];
        [self save_PoporNetRecord_webType:listWebType];
    }
}

- (void)updateShowListWeb_fromWebType {
    switch (_webType) {
        case PoporNetRecordAuto:
#if TARGET_IPHONE_SIMULATOR
            _showListWeb = YES;
#else
            //---------------- 2
#ifndef __OPTIMIZE__ //测试
            _showListWeb = YES;
#else //正式
            _showListWeb = NO;
#endif
            //---------------- 2
            
#endif
            break;
        case PoporNetRecordEnable:
            _showListWeb = YES;
            break;
            
        case PoporNetRecordDisable:
            _showListWeb = NO;
            break;
            
        default:
            break;
    }
}

- (void)save_PoporNetRecord_recordType:(PoporNetRecordType)type;
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%li", (long)type] forKey:PoporNetRecord_recordType];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (PoporNetRecordType)get_PoporNetRecord_recordType;
{
    NSString * info = [[NSUserDefaults standardUserDefaults] objectForKey:PoporNetRecord_recordType];
    return info.integerValue;
}

- (void)save_PoporNetRecord_webType:(PoporNetRecordType)type;
{
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%li", (long)type] forKey:PoporNetRecord_webType];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (PoporNetRecordType)get_PoporNetRecord_webType;
{
    NSString * info = [[NSUserDefaults standardUserDefaults] objectForKey:PoporNetRecord_webType];
    return info.integerValue;
}


- (BOOL)isRecord {
    return _record;
}

- (BOOL)isShowListWeb {
    return _showListWeb;
}

- (void)updateListCellHeight {
    self.listCellHeight = PnrListCellGap*3 + 6 + MAX(self.listFontTitle.pointSize, self.listFontRequest.pointSize) + self.listFontDomain.pointSize;
}

// 设定hexcolor
- (void)setListColorTitle:(UIColor *)listColorTitle {
    _listColorTitle = listColorTitle;
    _listColorTitleHex = [self hexStringColorNoAlpha:listColorTitle];
}

- (void)setListColorparameter:(UIColor *)listColorRequest {
    _listColorRequest = listColorRequest;
    _listColorRequestHex = [self hexStringColorNoAlpha:listColorRequest];
}

- (void)setListColorDomain:(UIColor *)listColorDomain {
    _listColorDomain = listColorDomain;
    _listColorDomainHex = [self hexStringColorNoAlpha:listColorDomain];
}

- (void)setListColorTime:(UIColor *)listColorTime {
    _listColorTime = listColorTime;
    _listColorTimeHex = [self hexStringColorNoAlpha:listColorTime];
}

- (void)setListColorCell0:(UIColor *)listColorCell0 {
    _listColorCell0 = listColorCell0;
    _listColorCell0Hex = [self hexStringColorNoAlpha:listColorCell0];
}

- (void)setListColorCell1:(UIColor *)listColorCell1 {
    _listColorCell1 = listColorCell1;
    _listColorCell1Hex = [self hexStringColorNoAlpha:listColorCell1];
}

- (void)setListWebColorCellBg:(UIColor *)listWebColorCellBg {
    _listWebColorCellBg = listWebColorCellBg;
    _listWebColorCellBgHex = [self hexStringColorNoAlpha:listWebColorCellBg];
}

- (void)setListWebColorCell0:(UIColor *)listWebColorCell0 {
    _listWebColorCell0 = listWebColorCell0;
    _listWebColorCell0Hex = [self hexStringColorNoAlpha:listWebColorCell0];
}

- (void)setListWebColorCell1:(UIColor *)listWebColorCell1 {
    _listWebColorCell1 = listWebColorCell1;
    _listWebColorCell1Hex = [self hexStringColorNoAlpha:listWebColorCell1];
}

- (void)setListWebColorBg:(UIColor *)listWebColorBg {
    _listWebColorBg = listWebColorBg;
    _listWebColorBgHex = [self hexStringColorNoAlpha:listWebColorBg];
}

- (void)setRootColorKey:(UIColor *)rootColorKey {
    _rootColorKey = rootColorKey;
    _rootColorKeyHex = [self hexStringColorNoAlpha:rootColorKey];
}

- (void)setRootColorValue:(UIColor *)rootColorValue {
    _rootColorValue = rootColorValue;
    _rootColorValueHex = [self hexStringColorNoAlpha:rootColorValue];
    
}

// tool
- (NSString *)hexStringColorNoAlpha:(UIColor *)color {
    //颜色值个数，rgb和alpha
    NSInteger cpts = (NSInteger)CGColorGetNumberOfComponents(color.CGColor);
    const CGFloat *components = CGColorGetComponents(color.CGColor);
    
    if (cpts == 2) {
        CGFloat w = components[0];//白色
        return [NSString stringWithFormat:@"#%02lX%02lX%02lX", lroundf(w * 255), lroundf(w * 255), lroundf(w * 255)];
    }else{
        CGFloat r = components[0];//红色
        CGFloat g = components[1];//绿色
        CGFloat b = components[2];//蓝色
        
        return [NSString stringWithFormat:@"#%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255)];
    }
}

//- (NSString *)hexStringColor: (UIColor*) color {
//    //颜色值个数，rgb和alpha
//    NSInteger cpts = CGColorGetNumberOfComponents(color.CGColor);
//    const CGFloat *components = CGColorGetComponents(color.CGColor);
//    CGFloat r = components[0];//红色
//    CGFloat g = components[1];//绿色
//    CGFloat b = components[2];//蓝色
//    if (cpts == 4) {
//        CGFloat a = components[3];//透明度
//        return [NSString stringWithFormat:@"#%02lX%02lX%02lX%02lX", lroundf(a * 255), lroundf(r * 255), lroundf(g * 255), lroundf(b * 255)];
//    } else {
//        return [NSString stringWithFormat:@"#%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255)];
//    }
//}

- (void)updateTextColorBlack:(NSInteger)color {
    [PnrConfig save__textColor:color];
    self.jsonViewColorBlack = color;
}

+ (void)save__textColor:(NSInteger)textColor {
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%li", (long)textColor] forKey:KeyTextColor];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (int)get__textColor {
    NSString * info = [[NSUserDefaults standardUserDefaults] objectForKey:KeyTextColor];
    if (info) {
        return [info intValue];
    }else{
        return PnrListTypeTextColor;
    }
}

- (void)updateLogDetail:(NSInteger)detail {
    [PnrConfig save__logDetail:detail];
    self.jsonViewLogDetail = detail;
}

+ (void)save__logDetail:(NSInteger)detail {
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%li", (long)detail] forKey:KeyLog];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+ (int)get__logDetail {
    NSString * info = [[NSUserDefaults standardUserDefaults] objectForKey:KeyLog];
    if (info) {
        return [info intValue];
    }else{
        return PnrListTypeLogDetail;
    }
}

@end
