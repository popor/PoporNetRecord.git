
//
//  PnrEntity.m
//  PoporNetRecord
//
//  Created by popor on 2018/5/16.
//  Copyright © 2018年 popor. All rights reserved.
//

#import "PnrEntity.h"
#import "PnrConfig.h"
#import "PnrPortEntity.h"
#import <JSONSyntaxHighlight/JSONSyntaxHighlight.h>

@implementation PnrEntity

- (void)createListWebH5:(NSInteger)index {
    PnrConfig * config = [PnrConfig share];

    NSString * bgColor = index%2==1 ? config.listWebColorCell0Hex:config.listWebColorCell1Hex;
    NSMutableString * h5 = [NSMutableString new];
    
    if (self.logValue) {
        // log 日志模式
        [h5 appendFormat:@"\n\n <div style=\" background:%@; width:100%%; height:%ipx; position:relative; \" onclick= \"parent.detail(%i);\" >", bgColor, PnrListHeight, (int)index];
        
        [h5 appendString:@"\n <div style=\" position:relative; width:100%%; top:4px; left:5px; \" >"];
        
        [h5 appendFormat:@"\n <div class='oneLine' > <font color='%@'>%i. %@ </font> <font color='%@'>%@  </font> </div>",  config.listColorTitleHex, (int)index,  self.title? : @"Log日志" , config.listColorRequestHex, @""];
        [h5 appendFormat:@"\n <div class='oneLine' >\n <font style=\" opacity:0.0; \" >%i. </font> <font color='%@'>%@  </font> <font color='%@'>%@ </font> </div>", (int)index, config.listColorTimeHex, self.time, config.listColorDomainHex, self.logValue];
        
        [h5 appendString:@"</div></div>"];
    }else{
        // 网络请求模式
        [h5 appendFormat:@"\n\n <div style=\" background:%@; width:100%%; height:%ipx; position:relative; \" onclick= \"parent.detail(%i);\" >", bgColor, PnrListHeight, (int)index];
        
        [h5 appendString:@"\n <div style=\" position:relative; width:100%%; top:4px; left:5px; \" >"];
        
        [h5 appendFormat:@"\n <div class='oneLine' > <font color='%@'>%i. %@ </font> <font color='%@'>%@  </font> </div>",  config.listColorTitleHex, (int)index, self.title , config.listColorRequestHex, [self.path substringToIndex:MIN(self.path.length, 80)]];
        [h5 appendFormat:@"\n <div class='oneLine' >\n <font style=\" opacity:0.0; \" >%i. </font> <font color='%@'>%@  </font> <font color='%@'>%@ </font> </div>", (int)index, config.listColorTimeHex, self.time, config.listColorDomainHex, self.domain];
        
        [h5 appendString:@"</div></div>"];
    }
    self.listWebH5 = [h5 copy];
}

- (NSArray *)titleArray {
    PnrEntity * entity = self;
    if (entity.logValue) {
        return @[entity.time, entity.logDic ? @"" : entity.logValue,];
    }else{
        NSString * title;
        if (entity.title) {
            title = [NSString stringWithFormat:@" %@\n%@", entity.title, entity.path];
        }else{
            title = [NSString stringWithFormat:@" \n%@",entity.path];
        }
        
        return @[[NSString stringWithFormat:@"%@\n%@", PnrRootPath1, title],
                 [NSString stringWithFormat:@"%@\n%@", PnrRootUrl2, entity.url],
                 [NSString stringWithFormat:@"%@\n%@", PnrRootTime3, entity.time],
                 [NSString stringWithFormat:@"%@\n%li - %@", PnrRootMethod4, (long)entity.method, entity.methodName],
                 
                 [NSString stringWithFormat:@"%@\n", PnrRootHead5],
                 [NSString stringWithFormat:@"%@\n", PnrRootParameter6],
                 [NSString stringWithFormat:@"%@\n", PnrRootResponse7],
        ];
    }
}

- (NSArray *)jsonArray {
    PnrEntity * entity = self;
    if (entity.logValue) {
        return @[[NSNull null], entity.logDic ?:entity.logValue,];
    }else{
        return @[[NSNull null],
                 [NSNull null],
                 [NSNull null],
                 [NSNull null],
                 
                 entity.headValue ?:[NSNull null],
                 entity.parameterValue ?:[NSNull null],
                 entity.responseValue ?:[NSNull null],
        ];
    }
}

- (void)getJsonArrayBlock:(PnrEntityBlock)finish {
    if (!finish) {
        return;
    }
    PnrConfig * config   = [PnrConfig share];
    NSArray * titleArray = [self titleArray];
    NSArray * jsonArray  = [self jsonArray];
    UIColor * textColor  = [UIColor darkGrayColor];
    
    NSMutableArray * cellAttArray = [NSMutableArray new];
    for (int i = 0; i<jsonArray.count; i++) {
        NSDictionary * json = jsonArray[i];
        
        NSMutableAttributedString * cellAtt = [[NSMutableAttributedString alloc] initWithString:titleArray[i] attributes:config.titleAttributes];
        
        if (json) {
            if ([json isKindOfClass:[NSDictionary class]]) {
                JSONSyntaxHighlight *jsh = [[JSONSyntaxHighlight alloc] initWithJSON:json];
                jsh.keyAttributes       = config.keyAttributes;
                jsh.stringAttributes    = config.stringAttributes;
                jsh.nonStringAttributes = config.nonStringAttributes;
                NSAttributedString * jsonAtt = [jsh highlightJSON];
                [cellAtt appendAttributedString:jsonAtt];
            }else if ([json isKindOfClass:[NSString class]]) {
                [PnrUITool att:cellAtt string:(NSString *)json font:nil color:textColor];
            }
        }
        
        [cellAttArray addObject:cellAtt];
    }
    
    finish(titleArray, jsonArray, cellAttArray);
}

- (NSDictionary *)desDic {
    NSMutableDictionary * dic = [NSMutableDictionary new];
    
    [dic setValue:self.logValue       forKey:@"logValue"];
    
    [dic setValue:self.title          forKey:@"title"];
    [dic setValue:self.url            forKey:@"url"];
    [dic setValue:self.domain         forKey:@"domain"];
    [dic setValue:self.path           forKey:@"path"];
    [dic setValue:@(self.method)      forKey:@"method"];

    [dic setValue:self.headValue      forKey:@"headValue"];
    [dic setValue:self.parameterValue forKey:@"parameterValue"];
    [dic setValue:self.responseValue  forKey:@"responseValue"];
    
    [dic setValue:self.time           forKey:@"time"];
    
    [dic setValue:self.deviceName     forKey:@"deviceName"];
    
    return dic;
}

@end

@implementation PnrEntityMethod

- (instancetype)initIndex:(NSInteger)index value:(NSString *)value {
    if (self = [super init]) {
        _index = index;
        _value = value;
    }
    return self;
}

@end
