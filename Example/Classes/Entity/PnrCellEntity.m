//
//  PnrCellEntity.m
//  PoporNetRecord_Example
//
//  Created by popor on 2019/6/4.
//  Copyright © 2019 popor. All rights reserved.
//

#import "PnrCellEntity.h"

@implementation PnrCellEntity

+ (PnrCellEntity *)type:(PnrListType)type title:(NSString *)title {
    PnrCellEntity * entity = [PnrCellEntity new];
    entity.type  = type;
    entity.title = title;
    
    return entity;
}

@end
