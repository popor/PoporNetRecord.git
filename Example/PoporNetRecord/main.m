//
//  main.m
//  PoporNetRecord
//
//  Created by popor on 07/06/2018.
//  Copyright (c) 2018 popor. All rights reserved.
//

@import UIKit;
#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
