//
//  PoporNetRecordViewController.m
//  PoporNetRecord
//
//  Created by popor on 07/06/2018.
//  Copyright (c) 2018 popor. All rights reserved.
//

#import "RootVC.h"

#import "PoporNetRecord.h"

#import <PoporAFN/PoporAFN.h>

#import "PnrExtraEntity.h"
#import "PnrUITool.h"
#import "PnrListVC.h"

@interface RootVC ()

@property (nonatomic        ) int netIndex;
@property (nonatomic        ) int logIndex;

@property (nonatomic, strong) UILabel * infoL;

@end

@implementation RootVC

- (void)viewDidLoad{
    [super viewDidLoad];
    self.title = @"";
    
    [self addViews];
    {
        UIBarButtonItem *item = [[UIBarButtonItem alloc] init];
        item.title = @"返回";
        self.navigationItem.backBarButtonItem = item;
    }
    
    // pnr 设置
    [self addPnrSettings];
    
    {
        UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"Push" style:UIBarButtonItemStylePlain target:self action:@selector(pushNetRecordListVC)];
        UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithTitle:@"Present" style:UIBarButtonItemStylePlain target:self action:@selector(presentNetRecordListVC)];
        
        self.navigationItem.leftBarButtonItems = @[item1, item2];
    }
    
    {
        UIBarButtonItem *item1 = [[UIBarButtonItem alloc] initWithTitle:@"  Net" style:UIBarButtonItemStylePlain target:self action:@selector(addOneNetRequest)];
        UIBarButtonItem *item2 = [[UIBarButtonItem alloc] initWithTitle:@"LogStr  " style:UIBarButtonItemStylePlain target:self action:@selector(addOneLog_str)];
        UIBarButtonItem *item3 = [[UIBarButtonItem alloc] initWithTitle:@"LogDic  " style:UIBarButtonItemStylePlain target:self action:@selector(addOneLog_dic)];
        self.navigationItem.rightBarButtonItems = @[item1, item2, item3];
    }
    
    [self addOneNetRequest];
    [self addOneLog_str];
    [self addOneLog_dic];
}

- (void)addPnrSettings {
    
    // 设置转发, 可以配合其他程序负责接收.
    [PnrExtraEntity shareConfig:^(PnrExtraEntity * _Nonnull pnrExtraEntity) {
        pnrExtraEntity.defaultForward = YES;// 默认是否转发
        pnrExtraEntity.defaultTitle   = @"默认本地:自定义";
        pnrExtraEntity.defaultUrl     = @"http://127.0.0.1";
        pnrExtraEntity.defaultPort    = @"9000";
        pnrExtraEntity.defaultApi     = @"api";
        
    }];
    
    PnrConfig * config = [PnrConfig share];
    {
        // 假如只需要在安装时候设置默认值, 允许后期更改
        [config setRecordTypeDefault:PoporNetRecordAuto]; // 重要
        [config setWebTypeDefault:PoporNetRecordDisable]; // 重要
        // else 假如需要每次启动都是用固定的配置,
        //config.recordType = PoporNetRecordAuto;
        //config.webType    = PoporNetRecordDisable;
    }
    
    {   // 网页
        // 网页 和 UI标题.
        config.webRootTitle = @"PnrTest";
        
        // 开启网页查看请求记录, 显示的网页ico
        NSString *path = [[NSBundle mainBundle] pathForResource:@"favicon" ofType:@"ico"];
        path = [[NSBundle mainBundle] pathForResource:@"favicon2" ofType:@"ico"];
        NSData *data   = [[NSData alloc] initWithContentsOfFile:path];
        config.webIconData = data;
    }
    
    {    // UI
        config.vcRootTitle  = @"";
        
        // 设置页面segmentedControl的颜色, 可以使用默认值.
        // config.segmentedControl_backgroundColor     = UIColor.clearColor;
        // config.segmentedControl_tintColor           = UIColor.whiteColor;
        // config.segmentedControl_titleColor_normal   = UIColor.blackColor;
        // config.segmentedControl_titleColor_selected = UIColor.blackColor;
    }
    
    __block NSString * pnrExtraForwadUrl;
    {// 请求方式设置
        PnrEntityMethod * m0 = [[PnrEntityMethod alloc] initIndex:0 value:@"Get"];
        PnrEntityMethod * m1 = [[PnrEntityMethod alloc] initIndex:1 value:@"POST(Json)"];
        PnrEntityMethod * m2 = [[PnrEntityMethod alloc] initIndex:2 value:@"Post(FormData)"];
        PnrEntityMethod * m3 = [[PnrEntityMethod alloc] initIndex:3 value:@"Post(XForm) "];
        PnrEntityMethod * m4 = [[PnrEntityMethod alloc] initIndex:4 value:@"Put"];
        
        [PoporNetRecord setMethodArray:@[m0, m1, m2, m3, m4]];
    }
    
    // 额外的回调
    [PoporNetRecord share].blockExtraRecord = ^(PnrEntity * entity){
        PnrExtraEntity * e = [PnrExtraEntity share];
        if (e.forward) {
            pnrExtraForwadUrl = e.selectUrlPort;
            if (![pnrExtraForwadUrl isEqualToString:entity.url]) {
                PoporAFN * afn = [PoporAFN new];
                [afn title:@""
                       url:pnrExtraForwadUrl
                    method:PoporMethodPostJson //PoporMethodPost
                     cache:PoporAfnCacheTypeNull
                parameters:entity.desDic
                    finish:nil];
                
                [afn run];
            }
        }
    };
    
    // 增加重新请求demo
    __block int record = 0;
    [PoporNetRecord setPnrBlockResubmit:^(NSDictionary *formDic, PnrBlockFeedback _Nonnull blockFeedback) {
        NSLog(@"resubmit request dic: %@", formDic);
        
        NSString * title        = formDic[@"title"];
        NSString * urlStr       = formDic[@"url"];
        NSInteger  method       = [formDic[@"method"] integerValue];
        NSString * headStr      = formDic[@"head"];
        NSString * parameterStr = formDic[@"parameter"];
        //NSString * extraStr   = formDic[@"extra"];
        
        // 将新的网络请求 数据存储到PoporNetRecord
        NSDictionary * resultDic = @{@"key": [NSString stringWithFormat:@"%i: %@", record++, @"新的返回数据:"]};
        resultDic = @{
            @"自定义重新请求":@"模拟数据",
            @"array":@[
            @{@"a":@"a111111111111", @"b":@"b2222222222222"},
            @{@"a":@"a111111111111", @"b":@"b2222222222222"},
            @{@"a":@"a111111111111", @"b":@"b2222222222222"},
            @{@"a":@"a111111111111", @"b":@"b2222222222222"},
            @{@"a":@"a111111111111", @"b":@"b2222222222222"},
            @{@"a":@"a111111111111", @"b":@"b2222222222222"},
            @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        ]};
        //[PoporNetRecord addUrl:urlStr title:title method:method head:headStr.toDic parameter:parameterStr.toDic response:resultDic];
        
        [PoporNetRecord addUrl:urlStr title:title method:method head:[PnrUITool dicOfJson:headStr] parameter:[PnrUITool dicOfJson:parameterStr] response:resultDic];
        // 结果反馈给PoporNetRecord
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            blockFeedback([PnrUITool jsonOfDic:resultDic]);
        });
    } extraDic:@{@"exKey":@"exValue"}];
    
    
    // 其他设置
    {
        //config.listFontTitle   = [UIFont systemFontOfSize:16];
        //config.listFontRequest = [UIFont systemFontOfSize:14];
        //[config updateListCellHeight];
    }
    {
        // config.recordType = PoporNetRecordDisable;
        // UIFont * font                = [UIFont systemFontOfSize:15];
        // // 假如att font 不为15,也需要设置,不然计算高度会出错.
        // config.cellTitleFont       = font;
        // config.titleAttributes     = @{NSForegroundColorAttributeName:[JSONSyntaxHighlight colorWithRGB:0x000000], NSFontAttributeName:font};
        // config.keyAttributes       = @{NSForegroundColorAttributeName:[JSONSyntaxHighlight colorWithRGB:0xE46F5C], NSFontAttributeName:font};
        // config.stringAttributes    = @{NSForegroundColorAttributeName:[JSONSyntaxHighlight colorWithRGB:0x4BB748], NSFontAttributeName:font};
        // config.nonStringAttributes = @{NSForegroundColorAttributeName:[JSONSyntaxHighlight colorWithRGB:0x4BB748], NSFontAttributeName:font};
    }
    
    // 网络请求死循环监测
    static double lastWatchTime = 0;
    lastWatchTime = [PnrUITool getCurrentUnixDate];
    [PoporNetRecord watchRequestRate:2 exception:^(CGFloat currentPerSecond, CGFloat maxPerSecond) {
        
        {   // UI 吐司之类的操作, 防止刷屏看不清楚
            double currentTime = [PnrUITool getCurrentUnixDate];
            if ((currentTime - lastWatchTime) > 2) {
                //AlertToastTitle(([NSString stringWithFormat:@"网络请求频率异常: %.01f > %.01f", currentPerSecond, maxPerSecond]));
                lastWatchTime = currentTime;
            }
        }
        NSLog(@"❌❌❌ 网络请求死循环监测 ❌❌❌");
    }];
    
}


- (void)addOneNetRequest {
    NSString * autoTitle = [NSString stringWithFormat:@"测试Net -- 比较长的名称 -- :%i", self.netIndex++ + 1];
    NSLog(@"auto title : %@ ", autoTitle);
    NSString * value = @"value";
    NSDictionary * pDic =
    @{@"array":@[
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        //--------
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        //--------
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        //--------
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        //--------
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        //--------
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        //--------
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        //--------
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        //--------
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        @{@"a":@"a111111111111", @"b":@"b2222222222222"},
        //--------
    
    ],
      @"b":@"b",
      @"f":@"f",
      @"c":@"c",
      @"a":@"a",
      @"x":@"x",
    };
    [PoporNetRecord addUrl:@"http://www.baidu.com/auto?a=a&b=b&c=c&d=d" title:autoTitle method:0 head:@{@"os":@"iOS", @"key":value} parameter:pDic response:@"responseText"];
    //AlertToastTitle(@"增加网路请求");
    NSLog(@"增加网路请求");
    
    self.infoL.text = @"增加网路请求";
    
    self.infoL.text = [NSString stringWithFormat:@"增加网路请求: %@", autoTitle];
    
}

- (void)addOneLog_str {
    NSString * autoTitle = [NSString stringWithFormat:@"测试        Log:%i", self.logIndex++ + 1];
    
    [PoporNetRecord addLog:@"new log, 1111111111, 2222222222, 3333333333, 4444444444, 5555555555." title:autoTitle];
    //AlertToastTitle(@"增加日志");
    
    NSLog(@"增加日志");
    self.infoL.text = [NSString stringWithFormat:@"增加日志: %@", autoTitle];
}

- (void)addOneLog_dic {
    NSString * autoTitle = [NSString stringWithFormat:@"测试        Log:%i", self.logIndex++ + 1];
    
    [PoporNetRecord addLog:@{@"key":autoTitle} title:autoTitle];
    //AlertToastTitle(@"增加日志");
    
    NSLog(@"增加日志");
    self.infoL.text = [NSString stringWithFormat:@"增加日志: %@", autoTitle];
}

- (void)pushNetRecordListVC {
    UIViewController * vc = [PnrListVC getPnrListVC_default];
    
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)presentNetRecordListVC {
    UIViewController * vc = [PnrListVC getPnrListVC_default];
    UINavigationController * nc = [[UINavigationController alloc] initWithRootViewController:vc];
    
    [self presentViewController:nc animated:YES completion:nil];
}

#pragma mark - views
- (void)addViews {
    self.infoL = ({
        UILabel * oneL = [UILabel new];
        oneL.frame               = CGRectMake(0, 0, 0, 44);
        oneL.backgroundColor     = [UIColor whiteColor]; // ios8 之前
        oneL.font                = [UIFont systemFontOfSize:15];
        oneL.textColor           = [UIColor blackColor];
        oneL.layer.masksToBounds = YES; // ios8 之后 lableLayer 问题
        oneL.numberOfLines       = 1;
        oneL.textAlignment       = NSTextAlignmentCenter;
        
        [self.view addSubview:oneL];
        oneL;
    });
    
    {
        UIView * view0     = self.infoL;
        UIView * superView = self.view;
        view0.translatesAutoresizingMaskIntoConstraints = NO;
        
        NSLayoutConstraint *centerXLC = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0];
        [superView addConstraint:centerXLC];
        
        NSLayoutConstraint *centerYLC  = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0];
        [superView addConstraint:centerYLC];
        
        NSLayoutConstraint *widthLC  = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:superView attribute:NSLayoutAttributeWidth multiplier:1 constant:1];
        [superView addConstraint:widthLC];
        
        NSLayoutConstraint *heightLC  = [NSLayoutConstraint constraintWithItem:view0 attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:40];
        [superView addConstraint:heightLC];
    }
}

@end
