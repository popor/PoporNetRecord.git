#ifdef __OBJC__
#import <UIKit/UIKit.h>
#else
#ifndef FOUNDATION_EXPORT
#if defined(__cplusplus)
#define FOUNDATION_EXPORT extern "C"
#else
#define FOUNDATION_EXPORT extern
#endif
#endif
#endif

#import "PoporWebServer.h"
#import "PoporWebServerConnection.h"
#import "PoporWebServerFunctions.h"
#import "PoporWebServerHTTPStatusCodes.h"
#import "PoporWebServerRequest.h"
#import "PoporWebServerResponse.h"
#import "PoporWebServerDataRequest.h"
#import "PoporWebServerFileRequest.h"
#import "PoporWebServerMultiPartFormRequest.h"
#import "PoporWebServerURLEncodedFormRequest.h"
#import "PoporWebServerDataResponse.h"
#import "PoporWebServerErrorResponse.h"
#import "PoporWebServerFileResponse.h"
#import "PoporWebServerStreamedResponse.h"

FOUNDATION_EXPORT double PoporWebServerVersionNumber;
FOUNDATION_EXPORT const unsigned char PoporWebServerVersionString[];

