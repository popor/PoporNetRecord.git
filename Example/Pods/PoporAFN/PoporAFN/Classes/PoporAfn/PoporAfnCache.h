//
//  PoporAfnCache.h
//  PoporAFN
//
//  Created by popor on 17/4/28.
//  Copyright © 2017年 popor. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSUInteger, PoporAfnCacheType) {
    PoporAfnCacheTypeNull,     // 不缓存
    PoporAfnCacheTypeDiskTime, // 缓存, 并且在 cacheTime 内
    PoporAfnCacheTypeDiskAll,  // 缓存, 并且忽略 cacheTime 限制
};

typedef void(^BlockP_PoporAfnCache0) (NSDictionary * _Nullable dic);
typedef void(^BlockP_PoporAfnCache1) (BOOL needUpdate);


@interface PoporAfnCache : NSObject

@property (nonatomic        ) CGFloat cacheTime;// 0 的话则忽略检查修改时间.

+ (instancetype)share;

// 为了安全请尽量在开发环境下使用, 防止一不小心在M1的cpu笔记本上面运行.
+ (void)setSimulatorCachePath:(NSString * _Nullable)simulatorCachePath;
+ (NSString *)simulatorCachePath;

+ (NSString *)basePath;

// tool
+ (NSDictionary *)dicOfKey:(NSString * _Nullable)key;

+ (BOOL)needSaveKey:(NSString * _Nullable)key text:(NSString * _Nullable)text;


//返回缓存数据时间戳
+ (NSTimeInterval)cacheTimeStampWithKey:(NSString *)key;

#pragma mark - 清空缓存数据
+ (void)clearCacheData;

// 假如发现缓存数据需要更新的话
+ (void)removeUrl:(NSString * _Nullable)url;

@end

NS_ASSUME_NONNULL_END
