//
//  PoporAFN.m
//  PoporAFN
//
//  Created by popor on 17/4/28.
//  Copyright © 2017年 popor. All rights reserved.
//

#import "PoporAFN.h"
#import <CommonCrypto/CommonCrypto.h>

//如何添加head.
//https://www.jianshu.com/p/c741236c5c30

@implementation PoporAFN

- (instancetype)init {
    if (self = [super init]) {
        self.isBlockFinshInMainQueue = YES;
        
    }
    return self;
}

- (void)title:(NSString *_Nullable)title
          url:(NSString *_Nullable)urlString
       method:(PoporMethod)method
        cache:(PoporAfnCacheType)cacheType
   parameters:(NSDictionary *_Nullable)parameters
       finish:(PoporAfnEntityBlock _Nullable)blockFinish
{
    self.title       = title;
    self.urlString   = urlString;
    self.method      = method;
    self.cacheType   = cacheType;
    self.parameters  = parameters;
    self.blockFinish = blockFinish;
}

- (void)run {
    NSString            * title            = self.title;
    NSString            * urlString        = self.urlString;
    PoporMethod           method           = self.method;
    PoporAfnCacheType     cacheType        = self.cacheType;
    NSDictionary        * parameters       = self.parameters;
    PoporAfnEntityBlock   blockFinish      = self.blockFinish;
    
    AFURLSessionManager * manager          = self.manager;
    NSDictionary        * header           = self.header;
    NSArray             * ignoreCacheArray = self.ignoreCacheArray;
    
    if (!manager) {
        manager = [PoporAfnConfig createManager];
    }
    
    if (!header) {
        header = [PoporAfnConfig createHeader];
    }
    
    PoporAfnEntity * pae = [PoporAfnEntity new];
    pae.title      = title;
    pae.url        = urlString;
    pae.method     = method;
    
    pae.parameters = parameters;
    pae.head       = header;
    pae.cacheType  = cacheType;
    
    // 假如需要缓存
    //if (cacheType != PoporAfnCacheTypeNull) {
    if (cacheType == PoporAfnCacheTypeDiskTime || cacheType == PoporAfnCacheTypeDiskAll) {
        pae.cacheKey         = [PoporAFN cacheUrl:urlString parameters:parameters];
        pae.ignoreCacheArray = ignoreCacheArray;
        
        NSDictionary * dic = [PoporAfnCache dicOfKey:pae.cacheKey];
        
        if (dic) {
            
            BOOL blockCacheInfo = NO;
            if (cacheType == PoporAfnCacheTypeDiskAll) { // 忽略时间限制
                blockCacheInfo = YES;
            } else { // 需要考虑超时限制
                NSTimeInterval timeStamp = [PoporAfnCache cacheTimeStampWithKey:pae.cacheKey];
                NSTimeInterval diffTime = [[NSDate date] timeIntervalSince1970] - timeStamp;
                if (diffTime < [PoporAfnCache share].cacheTime) {
                    blockCacheInfo = YES;
                }
            }
            
            
            if (blockCacheInfo) {
                PoporAfnEntity * paeCache = [PoporAfnEntity new];
                paeCache.title      = title;
                paeCache.url        = urlString;
                paeCache.method     = method;
                
                paeCache.parameters = parameters;
                paeCache.head       = header;
                paeCache.cacheType  = cacheType;
                
                paeCache.isCached   = YES;
                paeCache.success    = YES;
                paeCache.dic        = dic[@"dic"];
                
                pae.isCacheInfoBlocked = YES;
                
                if (blockFinish) {
                    if (self.isBlockFinshInMainQueue) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            blockFinish(paeCache);
                        });
                    } else {
                        blockFinish(paeCache);
                    }
                }
            }
        }
    }
    //__weak typeof(manager) weakManager = manager;
    switch(method) {
        case PoporMethodGet : {
            [(AFHTTPSessionManager *)manager GET:urlString parameters:parameters headers:header progress:self.blockProgress success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                pae.success = YES;
                [self manager:manager pae:pae task:task response:responseObject error:nil finish:blockFinish];
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                pae.success = NO;
                [self manager:manager pae:pae task:task response:nil error:error finish:blockFinish];
            }];
            break;
        }
        case PoporMethodPostJson : {
            [(AFHTTPSessionManager *)manager POST:urlString parameters:parameters headers:header progress:self.blockProgress success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                pae.success = YES;
                [self manager:manager pae:pae task:task response:responseObject error:nil finish:blockFinish];
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                pae.success = NO;
                [self manager:manager pae:pae task:task response:nil error:error finish:blockFinish];
            }];
            break;
        }
        case PoporMethodPostFormData: {
            
            [(AFHTTPSessionManager *)manager POST:urlString parameters:parameters headers:header constructingBodyWithBlock:self.blockPostFormData progress:self.blockProgress success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                pae.success = YES;
                [self manager:manager pae:pae task:task response:responseObject error:nil finish:blockFinish];
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                pae.success = NO;
                [self manager:manager pae:pae task:task response:nil error:error finish:blockFinish];
            }];
            
            // // 另一种方法: 抓包发现 这个方法的参数比较简洁
            // NSMutableURLRequest * request = [[AFHTTPRequestSerializer serializer] requestWithMethod:@"POST" URLString:urlString parameters:parameters error:nil];
            // for (NSString * key in header.keyEnumerator) {
            //     [request addValue:header[key] forHTTPHeaderField:key];
            // }
            //
            // __block NSURLSessionUploadTask * uploadTask;
            // uploadTask = [manager uploadTaskWithStreamedRequest:request progress:uploadProgress completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
            //     // if (error) {
            //     //     NSLog(@"Error: %@", error);
            //     // } else {
            //     //     NSLog(@"response:%@, responseObject:%@", response, responseObject);
            //     //     NSString * message = responseObject[@"message"];
            //     //     //NSString * str  = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
            //     //     NSLog(@"message: %@", message);
            //     // }
            //     if (!error) {
            //         [PoporAFN successManager:weakManager url:urlString title:title method:method parameters:parameters task:uploadTask response:responseObject success:success];
            //     } else {
            //         [PoporAFN failManager:weakManager url:urlString title:title method:method parameters:parameters task:uploadTask error:error failure:failure];
            //     }
            // }];
            //
            // [uploadTask resume];
            
            break;
        }
        case PoporMethodPostXform: {
            [(AFHTTPSessionManager *)manager POST:urlString parameters:parameters headers:header progress:self.blockProgress success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                pae.success = YES;
                [self manager:manager pae:pae task:task response:responseObject error:nil finish:blockFinish];
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                pae.success = NO;
                [self manager:manager pae:pae task:task response:nil error:error finish:blockFinish];
            }];
            break;
        }
        case PoporMethodPut : {
            [(AFHTTPSessionManager *)manager PUT:urlString parameters:parameters headers:header success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
                pae.success = YES;
                [self manager:manager pae:pae task:task response:responseObject error:nil finish:blockFinish];
            } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
                pae.success = NO;
                [self manager:manager pae:pae task:task response:nil error:error finish:blockFinish];
            }];
            
            break;
        }
    }
}

- (void)manager:(AFURLSessionManager *)manager
            pae:(PoporAfnEntity *)pae
           task:(NSURLSessionDataTask * _Nonnull)task
       response:(id _Nullable)responseObject
          error:(NSError * _Nullable)error
         finish:(PoporAfnEntityBlock _Nullable)finishBlock
{
    [manager invalidateSessionCancelingTasks:YES resetSession:NO];
    pae.task = task;
    
    if (responseObject) {
        pae.responseObject = responseObject;
    } else if (error) {
        pae.error = error;
        pae.dic   = @{@"code": @(error.code), @"异常":error.localizedDescription};
        
    } else {
        
    }
    
    void (^ afnBlock_Cache_finish_record)(void) = ^(void){
        if (finishBlock) {
            // 判断是否需要缓存
            if ((pae.cacheType == PoporAfnCacheTypeDiskTime || pae.cacheType == PoporAfnCacheTypeDiskAll)
                && pae.success)
            {
                
                // 检查是否需要缓存
                BOOL allowCache = YES;
                if (self.blockCacheCheck) {
                    allowCache = self.blockCacheCheck(pae);
                }
                
                // 允许缓存就缓存后回调, 否则直接回调.
                if (allowCache) {
                    BOOL needUpdate = [PoporAfnCache needSaveKey:pae.cacheKey text:pae.cacheText];
                    if (needUpdate || !pae.isCacheInfoBlocked) {
                        finishBlock(pae);
                    }
                } else {
                    finishBlock(pae);
                }
                
            } else {
                finishBlock(pae);
            }
        }
        
        // 增加记录
        PoporAfnEntityBlock recordBlock = [PoporAfnConfig share].recordBlock;
        if (recordBlock) {
            recordBlock(pae);
        }
    };
    
    if (self.isBlockFinshInMainQueue) {
        dispatch_async(dispatch_get_main_queue(), ^{
            afnBlock_Cache_finish_record();
        });
    } else {
        afnBlock_Cache_finish_record();
    }
    
}

#pragma mark - 下载
- (void)downloadUrl:(NSURL * _Nonnull)downloadUrl
        destination:(NSURL * _Nullable)destinationUrl
           progress:(nullable void (^)(float progress, NSProgress * _Nonnull downloadProgress))progressBlock
             finish:(nullable void (^)(NSURLResponse *response, NSURL *filePath, NSError *error))finishBlock
{
    //默认配置
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    
    //AFN3.0+基于封住URLSession的句柄
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    __weak typeof(manager) weakManager = manager;
    
    //请求
    NSURLRequest *request = [NSURLRequest requestWithURL:downloadUrl];
    
    //下载Task操作
    self.downloadTask = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
        if (progressBlock) {
            float progress = (float)downloadProgress.completedUnitCount/(float)downloadProgress.totalUnitCount;
            // 回到主队列刷新UI
            dispatch_async(dispatch_get_main_queue(), ^{
                progressBlock(progress, downloadProgress);
            });
        }
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        if (destinationUrl) {
            return destinationUrl;
        }else{
            NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) lastObject];
            NSString *path = [cachesPath stringByAppendingPathComponent:response.suggestedFilename];
            return [NSURL fileURLWithPath:path];
        }
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        __strong typeof(weakManager) strongSelf = weakManager;
        
        if (finishBlock) {
            finishBlock(response, filePath, error);
        }
        
        [strongSelf invalidateSessionCancelingTasks:YES resetSession:YES];
    }];
    
    [self.downloadTask resume];
}

#pragma mark - 额外工具
/// 设置默认的数据缓存,一般用于第一次
+ (void)addCache_url:(NSString *)urlString
                body:(NSDictionary *)body
                 dic:(NSDictionary *)dic
       onlyNullCache:(BOOL)isOnlyNullCache
{
    PoporAfnEntity  * pae  = [PoporAfnEntity new];
    pae.cacheKey = [PoporAFN cacheUrl:urlString parameters:body];
    pae.dic      = dic;
    
    if (isOnlyNullCache) {
        if ([PoporAfnCache dicOfKey:pae.cacheKey] != nil) {
            return;
        }
    }
    [PoporAfnCache needSaveKey:pae.cacheKey text:pae.cacheText];
}

+ (NSString *)cacheUrl:(NSString *)url parameters:(NSDictionary *)parameters {
    NSString * paraText = @"";
    if (parameters) {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:parameters options:kNilOptions error:nil];
        paraText =  [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        
        paraText = [self md5HashToLower32Bit:paraText]; // 防止文件名称超过255位
    }
    
    
    NSString * key = [NSString stringWithFormat:@"%@+%@", url, paraText];
    key = [key stringByReplacingOccurrencesOfString:@":" withString:@"_"];
    key = [key stringByReplacingOccurrencesOfString:@"/" withString:@"'"];
    
    if (key.length >= 255) {
        key = [self md5HashToLower32Bit:key]; // 防止文件名称超过255位
    }
    return key;
}

//作者：你好哈喽哈喽
//链接：https://www.jianshu.com/p/6d3daebfd3a3
//来源：简书
//著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
+ (NSString *)md5HashToLower32Bit:(NSString *)text {
    const char *input = [text UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(input, (CC_LONG)strlen(input), result);
    
    NSMutableString *digest = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for (NSInteger i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [digest appendFormat:@"%02x", result[i]];
    }
    return digest;
}

@end
