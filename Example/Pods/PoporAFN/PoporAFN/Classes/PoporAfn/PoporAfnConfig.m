//
//  PoporAFNConfig.m
//  PoporAFN
//
//  Created by popor on 17/4/28.
//  Copyright © 2017年 popor. All rights reserved.
//

#import "PoporAfnConfig.h"

#define SHARE_INSTANCE_WANZI \
+ (id)sharedInstance \
{\
static dispatch_once_t once;\
static id instance;\
dispatch_once(&once, ^{instance = [self new];});\
return instance;\
}

@implementation PoporAfnConfig

// 有内存泄露问题弃用
//+ (instancetype)share {
//    static dispatch_once_t once;
//    static AFNServerConfig * instance;
//    dispatch_once(&once, ^{
//        instance = [self new];
//        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//
//        manager.requestSerializer =  [AFJSONRequestSerializer serializer];
//        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", nil]; // 不然不支持www.baidu.com.
//
//        manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
//
//        manager.requestSerializer.timeoutInterval = 10.0f;
//
//        instance.manager = manager;
//    });
//    return instance;
//}

+ (PoporAfnConfig *)share {
    static dispatch_once_t once;
    static PoporAfnConfig * instance;
    dispatch_once(&once, ^{
        instance = [self new];
        
    });
    return instance;
}

+ (AFHTTPSessionManager *)createManager {
    PoporAfnConfig * config = [PoporAfnConfig share];
    if (config.afnSMBlock) {
        return config.afnSMBlock();
    }else{
        AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
        
        manager.requestSerializer     = [AFJSONRequestSerializer serializer];
        manager.responseSerializer    = [AFHTTPResponseSerializer serializer];
        
        manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/plain", @"text/html" , nil];
        
        [manager.requestSerializer setValue:@"iOS" forHTTPHeaderField:@"system"];
        
        manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
        
        manager.requestSerializer.timeoutInterval = 10.0f;
        
        return manager;
    }
}

+ (NSDictionary *)createHeader {
    PoporAfnConfig * config = [PoporAfnConfig share];
    if (config.afnHeaderBlock) {
        return config.afnHeaderBlock();
    }else{
        return nil;
    }
}

#pragma mark - manange
+ (AFURLSessionManager *)postFormDataManager {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    // request
    manager.requestSerializer  = [AFHTTPRequestSerializer serializer];
    
    // response
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/html", @"text/plain",  @"image/jpeg", @"image/png", @"application/octet-stream", @"multipart/form-data", nil];
    
    manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    manager.requestSerializer.timeoutInterval = 15.0f;
    return manager;
}

+ (AFURLSessionManager *)postXformManager {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    // request
    manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [manager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    // response
    //    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    //    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/html", @"text/plain",  @"image/jpeg", @"image/png", @"application/octet-stream", @"multipart/form-data", nil];
    
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", @"text/plain",nil];
    
    manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    manager.requestSerializer.timeoutInterval = 15.0f;
    return manager;
}

+ (AFURLSessionManager *)jsonManager {
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    // request
    manager.requestSerializer  = [AFJSONRequestSerializer serializer];
    
    // response
    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html", @"text/plain", nil]; // 不然不支持www.163.com.
    
    manager.completionQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    manager.requestSerializer.timeoutInterval = 10.0f;
    return manager;
}

#pragma mark -

+ (NSString *)titleOfUrl:(NSString *)url
{
    NSString * title = url.lastPathComponent;
    NSRange range = [title rangeOfString:@"?"];
    if (range.location != NSNotFound) {
        title = [title substringToIndex:range.location];
    }
    
    return title;
}

@end

