//
//  PoporAfnChain.m
//  PesoMax
//
//  Created by maczh on 2023/8/31.
//

#import "PoporAfnChain.h"
#import "PoporAfnConfig.h"
#import "PoporAFN.h"

static PoporAFNBlock_PAfn kBlock_editInitAfn = nil;
static Block_PAF_string   kBlock_editTitle   = nil;

@implementation PoporAfnChain

//- (void)dealloc {
//    NSLogStr(@"✅✅ NetChain 销毁");
//}

- (instancetype)init {
    if (self = [self initMethod:PoporMethodGet afn:nil]) {
        
    }
    return self;
}

- (instancetype)initMethod:(PoporMethod)method afn:(PoporAFN * _Nullable)afn {
    if (self = [super init]) {
        if (afn == nil) {
            afn = [PoporAFN new];
            if (method == PoporMethodPostFormData) {
                afn.manager = [PoporAfnConfig postFormDataManager];
            } else if (method == PoporMethodPostXform) {
                afn.manager = [PoporAfnConfig postXformManager];
            } else {
                afn.manager = [PoporAfnConfig jsonManager];
            }
            _afn = afn;
        } else {
            _afn = afn;
        }
        afn.method = method;
        
        /// 如果需要修改afn的缓存等方法, 可以再次调用
        if (kBlock_editInitAfn) {
            kBlock_editInitAfn(afn);
        }
    }
    return self;
}

/// 设置在init时候, 修改afn配置的方法
+ (void)setBlock_editInitAfn:(PoporAFNBlock_PAfn)block {
    kBlock_editInitAfn = block;
}

- (PoporAfnChain *(^)(NSString *))title {
    return ^PoporAfnChain *(NSString * title){
#ifndef __OPTIMIZE__ //测试
        if (kBlock_editTitle) {
            title = kBlock_editTitle(title);
        }
#else //正式
        
#endif
        self.afn.title = title;
        return self;
    };
}

+ (void)setBlock_editTitle:(Block_PAF_string)block {
    kBlock_editTitle = block;
}

- (PoporAfnChain *(^)(NSString *))url {
    return ^PoporAfnChain *(NSString * url){
        if (!self.afn.title) {
            self.afn.title = [PoporAfnConfig titleOfUrl:url];
        }
        self.afn.urlString = url;
        return self;
    };
}

- (PoporAfnChain *(^)(NSDictionary * _Nullable))body {
    return ^PoporAfnChain *(NSDictionary * _Nullable body){
        self.afn.parameters = body;
        return self;
    };
}

- (PoporAfnChain *(^)(NSDictionary * _Nullable))parameters {
    return ^PoporAfnChain *(NSDictionary * _Nullable body){
        self.afn.parameters = body;
        return self;
    };
}

- (PoporAfnChain *(^)(PoporAfnCacheType))cache {
    return ^PoporAfnChain *(PoporAfnCacheType cache){
        self.afn.cacheType = cache;
        return self;
    };
}

- (PoporAfnChain *(^)(BOOL))mainThread {
    return ^PoporAfnChain *(BOOL isMain){
        self.afn.isBlockFinshInMainQueue = isMain;
        return self;
    };
}

- (PoporAfnChain *(^)(PoporAFNBlock_PDicPae _Nullable))success {
    return ^PoporAfnChain *(PoporAFNBlock_PDicPae _Nullable block){
        self.blockSuccess = block;
        return self;
    };
}
- (PoporAfnChain *(^)(PoporAFNBlock_PErrorPae _Nullable))failure {
    return ^PoporAfnChain *(PoporAFNBlock_PErrorPae _Nullable block){
        self.blockFailure = block;
        return self;
    };
}

- (PoporAfnChain *(^)(PoporAFNBlock_PAfn _Nullable))edit {
    return ^PoporAfnChain *(PoporAFNBlock_PAfn _Nullable block){
        self.blockEidtAfn = block;
        return self;
    };
}

/// 开始执行
- (PoporAfnChain *(^)(void))run {
    return ^PoporAfnChain *(void){
        [self request];
        return self;
    };
}

- (void)request {
    PoporAFNBlock_PDicPae   success = self.blockSuccess;
    PoporAFNBlock_PErrorPae failure = self.blockFailure;
    
    void (^ successExtra)(PoporAfnEntity *) = ^(PoporAfnEntity * _Nonnull pae){
        if (success) {
            success(pae.dic, pae);
        }
        if ([PoporAfnConfig share].extraSuccessBlock) {
            [PoporAfnConfig share].extraSuccessBlock(pae.dic, pae);
        }
    };
    
    PoporAFNBlock_PErrorPae failureExtra = ^(NSError * error, PoporAfnEntity * _Nonnull pae) {
        if (failure) {
            failure(error, pae);
        }
        if ([PoporAfnConfig share].extraFailBlock) {
            [PoporAfnConfig share].extraFailBlock(error, pae);
        }
    };
    
    self.afn.blockFinish = ^(PoporAfnEntity * _Nonnull pae) {
        if (pae.error) {
            failureExtra(pae.error, pae);
        } else if(pae.dic) {
            successExtra(pae);
            
        } else {
            if (pae.success) {
                pae.dic = @{@"NetServiceMessage":@"message null"};
                successExtra(pae);
                
            } else {
                static NSError *error;
                if (!error) {
                    NSString     *domain   = @"com";
                    NSString     *desc     = NSLocalizedString(@"nothing get", @"");
                    NSDictionary *userInfo = @{ NSLocalizedDescriptionKey : desc };
                    
                    error = [NSError errorWithDomain:domain code:11111 userInfo:userInfo];
                }
                
                failureExtra(error, pae);
            }
        }
    };
    
    if (self.blockEidtAfn) {
        self.blockEidtAfn(self.afn);
    }
    
    [self.afn run];
}

@end
