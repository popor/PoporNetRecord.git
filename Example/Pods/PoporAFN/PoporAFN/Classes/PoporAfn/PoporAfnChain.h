//
//  PoporAfnChain.h
//  PesoMax
//
//  Created by maczh on 2023/8/31.
//

#import <Foundation/Foundation.h>
@class PoporAFN;
#import "PoporAfnEntity.h"

NS_ASSUME_NONNULL_BEGIN

typedef NSString * _Nonnull(^Block_PAF_string) (NSString * string);

@interface PoporAfnChain : NSObject

@property (nonatomic, strong) PoporAFN * afn;

@property (nonatomic, copy  ) PoporAFNBlock_PDicPae   blockSuccess;
@property (nonatomic, copy  ) PoporAFNBlock_PErrorPae blockFailure;
@property (nonatomic, copy  ) PoporAFNBlock_PAfn      blockEidtAfn;


/// 最好是afn和method都提供,否则只能用默认方案
- (instancetype)initMethod:(PoporMethod)method afn:(PoporAFN * _Nullable)afn;

/// 设置在init时候, 修改afn配置的方法, 适用于公共配置
///
/// 1. 设置忽略的缓存数组
///
///        afn.ignoreCacheArray = @[@"request_id"];
///
/// 2. 是否缓存开关
///
///        afn.blockCacheCheck = ^BOOL(PoporAfnEntity * _Nonnull pae) {
///            NSString * code = [pae.dic valueForKey:@"code"];
///
///            if (code && [code intValue] == 200 ) {
///                return YES;
///            } else {
///                return NO;
///            }
///        };
///
+ (void)setBlock_editInitAfn:(PoporAFNBlock_PAfn)block;


- (PoporAfnChain *(^)(NSString *))title;

/// 设置自定义的title修改方案, 只在debug模式生效
///
/// 1. iOS端示例
///
///         if ([NSThread currentThread] == [NSThread mainThread]) {
///             UINavigationController * nc = [NetBase rootNC_netService];
///             UIViewController       * vc = [nc topViewController];
///             if (vc) {
///                 title = [NSString stringWithFormat:@"%@- %@", title, NSStringFromClass([vc class])];
///             }
///         }
///
+ (void)setBlock_editTitle:(Block_PAF_string)block;


- (PoporAfnChain *(^)(NSString *))url;

/// body 对应之前的 parameters, 这个单词用的不顺手
- (PoporAfnChain *(^)(NSDictionary * _Nullable))body;
- (PoporAfnChain *(^)(NSDictionary * _Nullable))parameters;

/// 缓存策略
- (PoporAfnChain *(^)(PoporAfnCacheType))cache;

/// 是否在主线程返回数据, 有些极端情况需要设置为NO, 比如程序崩溃之后发送的数据只能在非主线程返回(此时主线程已经被系统锁死了)
- (PoporAfnChain *(^)(BOOL))mainThread;

- (PoporAfnChain *(^)(PoporAFNBlock_PDicPae _Nullable))success;

- (PoporAfnChain *(^)(PoporAFNBlock_PErrorPae _Nullable))failure;


/// 修改afn参数, 如增加图片数据, 修改afn.header.
///
/// 1. 添加上传图片等信息
///
///         edit (^(PoporAFN * _Nonnull afn) {
///             afn.blockPostFormData = ^(id<AFMultipartFormData>  _Nonnull formData) {
///                 [formData appendPartWithFileData:imageData name:@"imgFile" fileName:@"imgFile.jpg" mimeType:@"image/jpg"];
///             };
///         });
///
/// 2. 修改默认的header数据
///
///         .edit (^(PoporAFN * _Nonnull afn) {
///             NSMutableDictionary * header = [[PoporAfnConfig createHeader] mutableCopy];
///             [header setValue:@"2" forKey:@"api-version"];
///             afn.header = header;
///         })
///
/// 3. 设置上传进度
///
///         afn.blockProgress = ^(NSProgress * _Nonnull uploadProgress) {};
///
/// 4. 清空seesionId
///
///         NSArray *cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookiesForURL:[NSURL URLWithString:UrlMain(@"")]];
///         for (NSHTTPCookie *cookie in cookies){
///             [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
///         }
///
- (PoporAfnChain *(^)(PoporAFNBlock_PAfn _Nullable))edit;


/// 开始执行
- (PoporAfnChain *(^)(void))run;


@end

NS_ASSUME_NONNULL_END
