//
//  PoporAFN.h
//  PoporAFN
//
//  Created by popor on 17/4/28.
//  Copyright © 2017年 popor. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PoporAfnConfig.h"
#import "PoporAfnEntity.h"
#import "PoporAfnCache.h"
#import "PoporAfnChain.h"

#import "AFHTTPSessionManager.h"

NS_ASSUME_NONNULL_BEGIN

@interface PoporAFN : NSObject

#pragma mark - 普通函数请求, 推荐使用PoporAfnChain请求
- (void)title:(NSString *_Nullable)title
          url:(NSString *_Nullable)urlString
       method:(PoporMethod)method
        cache:(PoporAfnCacheType)cacheType
   parameters:(NSDictionary *_Nullable)parameters
       finish:(PoporAfnEntityBlock _Nullable)blockFinish;

- (void)run;

#pragma mark - 一些额外的请求前参数设置
// 单独拎出来原因: 用的太多
@property (nonatomic, copy  ) NSString                  * title;
@property (nonatomic, copy  ) NSString                  * urlString;
@property (nonatomic        ) PoporMethod                 method;
@property (nonatomic        ) PoporAfnCacheType           cacheType;
@property (nonatomic, copy  ) NSDictionary              * parameters;
@property (nonatomic, copy  ) PoporAfnEntityBlock         blockFinish;
@property (nonatomic        ) BOOL                        isBlockFinshInMainQueue; // 回调在主线程, 默认为YES.

// 单独拎出来原因: 用的太少
@property (nonatomic, copy  ) PoporAFNBlock_PPostFormData blockPostFormData;
@property (nonatomic, copy  ) PoporAFNBlock_PNSProgress   blockProgress;

// 单独拎出来原因: 几乎都一样
@property (nonatomic, copy  ) NSArray                   * ignoreCacheArray; //需要移除忽略的dic某个参数数组
@property (nonatomic, copy  ) PoporAFNBlock_PPaeRBool     blockCacheCheck;
@property (nonatomic, strong) AFURLSessionManager       * manager;
@property (nonatomic, copy  ) NSDictionary              * header;

#pragma mark - 下载
@property (nonatomic, strong) NSURLSessionDownloadTask  * downloadTask;

// PoporAFN 需要持久化,否则无法下载
- (void)downloadUrl:(NSURL * _Nonnull)downloadUrl
        destination:(NSURL * _Nullable)destinationUrl
           progress:(nullable void (^)(float progress, NSProgress * _Nonnull downloadProgress))progressBlock
             finish:(nullable void (^)(NSURLResponse *response, NSURL *filePath, NSError *error))finishBlock;


#pragma mark - 额外工具
/// 设置默认的数据缓存,一般用于第一次
+ (void)addCache_url:(NSString *)urlString
                body:(NSDictionary *)body
                 dic:(NSDictionary *)dic
       onlyNullCache:(BOOL)isOnlyNullCache;

+ (NSString *)cacheUrl:(NSString *)url parameters:(NSDictionary *)parameters;

//作者：你好哈喽哈喽
//链接：https://www.jianshu.com/p/6d3daebfd3a3
//来源：简书
//著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。
+ (NSString *)md5HashToLower32Bit:(NSString *)text;

@end

NS_ASSUME_NONNULL_END
