#
# Be sure to run `pod lib lint PoporNetRecord.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name     = 'PoporNetRecord'
  s.version  = '2.13'
  s.summary  = 'PoporNetRecord will record net request only in debug configuration'
  
  s.homepage = 'https://gitee.com/popor/PoporNetRecord'
  s.license  = { :type => 'MIT', :file => 'LICENSE' }
  s.author   = { 'popor' => '908891024@qq.com' }
  s.source   = { :git => 'https://gitee.com/popor/PoporNetRecord.git', :tag => s.version.to_s }
  
  s.ios.deployment_target = '9.0'
  s.frameworks = 'UIKit', 'Foundation'
  
  s.subspec 'Entity' do |ss|
    ss.source_files = 'Example/Classes/Entity/*.{h,m}'
  end
  
  s.subspec 'WebServer' do |ss|
    ss.dependency 'PoporNetRecord/Entity'
    ss.source_files = 'Example/Classes/WebServer/*.{h,m}'
  end
  
  s.subspec 'Record' do |ss|
    ss.dependency 'PoporNetRecord/WebServer'
    
    ss.source_files = 'Example/Classes/Record/*.{h,m}'
  end
  
  s.dependency 'JSONSyntaxHighlight'
  #s.dependency 'GCDWebServer'
  s.dependency 'PoporWebServer'
  
  
end
